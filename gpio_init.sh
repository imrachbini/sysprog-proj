#! /bin/bash

gpio mode 17 out # LED
gpio mode 27 out # LED

gpio mode 23 in # PIR sensor
gpio mode 24 out # Alarm

exit 0
