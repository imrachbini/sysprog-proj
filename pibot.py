import sys
import time
import datetime
import telepot
import RPi.GPIO as GPIO
from picamera import PiCamera
from telepot.loop import MessageLoop
from subprocess import call

def handle(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    print(content_type, chat_type, chat_id)

    if content_type == 'text':
        if msg['text'] == '/connect':
            connected_user.append(chat_id)
            bot.sendMessage(chat_id, 'You are connected.')
        if msg['text'] == '/status':
            if chat_id in connected_user:
                bot.sendMessage(chat_id, 'You are connected.')
            else:
                bot.sendMessage(chat_id, 'You are disconnected.')
        if msg['text'] == '/disconnect':
            connected_user.remove(chat_id)
            bot.sendMessage(chat_id, 'You are disconnected.')
            
TOKEN = "453394843:AAG3nf5TXyHudAGTq6Zy6WaUn2ByvjflZ5I"

connected_user = []
bot = telepot.Bot(TOKEN)
MessageLoop(bot, handle).run_as_thread()

camera = PiCamera()
camera.rotation = 180

converter = "MP4Box -add video.h264 video.mp4"

GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN) #PIR
GPIO.setup(24, GPIO.OUT) #Buzzer

print('Listening ...')

# Keep the program running.
try:
    time.sleep(2) # to stabilize sensor
    while 1:      
        if GPIO.input(23):
            print("Motion Detected...")
            for usr in connected_user:
                bot.sendMessage(usr, 'Motion Detected! {}'.format(datetime.datetime.now()))
            camera.start_recording('video.h264')
            GPIO.output(24, True)
            time.sleep(5) #Buzzer turns on for 4 sec
            GPIO.output(24, False)
            camera.stop_recording()
	    call([converter], shell=True)
            for usr in connected_user:
                bot.sendVideo(usr, video=open('video.mp4', 'rb'))
	    print("done!")
            time.sleep(5) #to avoid multiple detection
        
        time.sleep(0.1) #loop delay, should be less than detection delay
except:
    GPIO.cleanup()