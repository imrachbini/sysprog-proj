#!/bin/sh

### BEGIN INIT INFO
# Provides:          hostapd
# Required-Start:
# Required-Stop:
# Should-Start:
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: Hostapd Service
# Description:       Intended to start hostapd on system startup.
### END INIT INFO

set -e

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

case "$1" in
    start)
        /usr/sbin/hostapd -B
        ;;
    *)
        echo "Usage: $NAME {start}" >&2
        exit 3
        ;;
esac